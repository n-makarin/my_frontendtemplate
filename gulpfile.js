const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const browserSync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps'); 
const gcmq = require('gulp-group-css-media-queries'); 
const preproc = require('gulp-sass');
const rigger = require('gulp-rigger');


const config = {
    src: 'build/',
    css: {
        src: 'source/scss/styles.scss',
        watch: 'source/scss/**/*.scss',
        dest: 'web/css/'
    },
    html: {
        src: 'source/index.html',
        watch: 'source/**/*.html',
        dest: 'web/'
    },
    js: {
        watch: 'source/js/**/*.js',
        dest: 'web/js/'
    }
};




gulp.task('build', function () {

    gulp.src(config.src + config.html.src) // config.html.src
        .pipe(rigger())
        .pipe(browserSync.reload({ stream: true }))
        .pipe(gulp.dest(config.src + config.html.dest));

    gulp.src(config.src + config.css.src)
        .pipe(preproc())
        .pipe(gcmq())
        .pipe(autoprefixer({
            browsers: ['> 0.1%'],
            cascade: false
        }))
        .pipe(cleanCSS({
                level: 2  
            }))
        .pipe(sourcemaps.init())
        .pipe(sourcemaps.write('.'))
        .pipe(browserSync.reload({ stream: true }))
        .pipe(gulp.dest(config.src + config.css.dest));

    gulp.src(config.src + config.js.watch)
        .pipe(browserSync.reload({ stream: true }))
        .pipe(gulp.dest(config.src + config.js.dest));

    gulp.src('build/source/images/**')
        .pipe(browserSync.reload({ stream: true }))
        .pipe(gulp.dest('build/web/images/'));
});

gulp.task('w', ['build', 'browserSync'], function () {
    gulp.watch(config.src + config.css.watch, ['build'], browserSync.reload);
    gulp.watch(config.src + config.html.watch, ['build'], browserSync.reload);
    gulp.watch(config.src + config.js.watch, ['build'], browserSync.reload);
    gulp.watch('build/source/images/**', ['build'], browserSync.reload);
});


gulp.task('browserSync', function () {
    browserSync.init({
        server: {
            baseDir: 'build/web/'
        }
    });
});